import { getLength } from "@remotion/paths";
import { useEffect, useRef } from "react";
import { interpolate, useCurrentFrame, useVideoConfig } from "remotion";
import rough from "roughjs";

const splitPath = (pathDescription: string):Array<string> => {
  // Lazy parsing split on the start of the Move command (M), I add it back in later
  const pathParts = pathDescription.split("M");
  // The command starts with an M so first result of split will be empty string, skipping first element when mapping
  return pathParts.slice(1).map((path) => `M${path}`);
}

export const Circle : React.FC = () => {
  const {width, height} = useVideoConfig();
  const frame = useCurrentFrame();
  const svgRef = useRef(null);

  const outlineProgress = interpolate(
    frame,
    [0, 60],
    [0.0, 1.0],
    {
      extrapolateRight: 'clamp'
    });

    const fillProgress = interpolate(
      frame,
      [30, 90],
      [0.0, 1.0],
      {
        extrapolateLeft: 'clamp',
        extrapolateRight: 'clamp',
      });

  useEffect(() => {
    const roughSvg = rough.svg(svgRef.current);
    const circleGroup = roughSvg.circle(width/2, height/2, 480, {
      strokeWidth: 10,
      fill: '#FF0000',
      fillWeight: 10, 
      seed: 42,
      disableMultiStroke: false,
      disableMultiStrokeFill: true,
    });
    // Assuming / hoping that this is deterministic
    const outlinePath = circleGroup.lastChild;
    const outlinePathDefinition = outlinePath.getAttribute("d");
    const outlineLength = getLength(outlinePathDefinition);
    outlinePath.setAttribute("stroke-dasharray", outlineLength);
    outlinePath.setAttribute("stroke-dashoffset", outlineLength - outlineLength * outlineProgress);

    const fillPath = circleGroup.firstChild;
    const fillPathDefinition = fillPath.getAttribute("d");

    const individualPaths = splitPath(fillPathDefinition);
    const individualPathLengths = individualPaths.map((pathDescription) => getLength(pathDescription));
    const totalPathsLength = individualPathLengths.reduce((acc, val) => acc+val, 0);

    const totalDashoffset = totalPathsLength - totalPathsLength * fillProgress;

    var cummulativeLength = 0;

    const linePaths = individualPaths.map((path, index) => {
      const individualLength = individualPathLengths[index];
      cummulativeLength += individualLength;
      const fudge = totalPathsLength - cummulativeLength;
      const individualDashoffset = Math.max(0, Math.min(individualLength, totalDashoffset - fudge));

      const node = fillPath?.cloneNode();
      node.setAttribute("d", path);
      node.setAttribute("stroke-dasharray", individualLength);
      node.setAttribute("stroke-dashoffset", individualDashoffset);
      return node;
    });

    fillPath?.replaceWith(...linePaths);
    
    svgRef.current.appendChild(circleGroup);

    return () => {
      svgRef.current.replaceChildren(); 
    }
  }, [outlineProgress, fillProgress]);

  return (<svg ref={svgRef} viewBox={`0 0 ${width} ${height}`}></svg>)
};
