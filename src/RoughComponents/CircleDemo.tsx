import { AbsoluteFill } from "remotion";
import { Circle } from "./Circle";

export const CircleDemo: React.FC = () => {
  return (<AbsoluteFill style={{backgroundColor: 'white'}}>
    <Circle/>
  </AbsoluteFill>);
}
