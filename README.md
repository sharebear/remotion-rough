# Remotion Rough

This repository contains the code from my experiment trying to incrementally
draw [Rough.js][rough-js] components in a [Remotion][remotion] video project.

This is not production ready code, and I may or may not find the time to develop
this into a reusable set of components... lets see what 2024 brings.

[rough-js]: https://roughjs.com/
[remotion]: https://www.remotion.dev/
